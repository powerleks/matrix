var add_row = function(matrix, matrix_name, b_add_row, b_del_row) {
    var row = [];
    var p = document.createElement('p');
    p.id = matrix_name.id[5] + (matrix.length + 1);
    for (var i = 0; i < matrix[0].length; ++i) {
        row.push(0);

        var cell = document.createElement('input');
        if (matrix_name.id == 'matr_c' || document.getElementById('left').style.backgroundColor == "rgb(246, 193, 192)") {
            cell.readOnly = true;
            cell.disabled = true;
        }
        cell.id = p.id + ',' + (i + 1);
        cell.value = cell.id;
        cell.style.color = "gray";
        cell.onfocus = function() {
            if (this.value == this.id) {
                this.value = "";
                this.style.color = "black";
            }
            this.style.border = '1px solid #529ADB';
            this.style.outline = '1px solid #529ADB';
            this.style.boxShadow = 'inset 0 1px 0 white';
            document.getElementById('left').style.backgroundColor = "#5199DB";
        }
        cell.onblur = function() {
            if (this.value == "") {
                this.value = this.id;
                this.style.color = "gray";
            }
            this.style.border = '1px solid #D9D9D9';
            this.style.outline = '';
            this.style.boxShadow = 'inset 0 1px 0 #B2B2B2';
            document.getElementById('left').style.backgroundColor = "#BCBCBC";
        }
        p.appendChild(cell);
    }
    matrix.push(row);
    matrix_name.insertBefore(p, matrix_name.children[matrix_name.children.length - 1]);
    
    if (matrix.length == 10) {
        b_add_row.style.color = 'gray';
        b_add_row.style.cursor = 'default';
        b_add_row.id = 'add_row_disable';
        b_add_row.tabIndex = '-1';
    } else {
        b_del_row.style.color = 'black';
        b_del_row.style.cursor = 'pointer';
        b_del_row.id = 'del_row';
        b_del_row.tabIndex = '0';
    }
}

var del_row = function(matrix, matrix_name, b_add_row, b_del_row) {
    matrix.pop();

    var id = matrix_name.id[5] + (matrix.length + 1);
    var p = document.getElementById(id)
    matrix_name.removeChild(p);

    if (matrix.length == 2) {
        b_del_row.style.color = 'gray';
        b_del_row.style.cursor = 'default';
        b_del_row.id = 'del_row_disable'
        b_del_row.tabIndex = '-1';
    } else {
        b_add_row.style.color = 'black';
        b_add_row.style.cursor = 'pointer';
        b_add_row.id =  'add_row';
        b_add_row.tabIndex = '0';
    }
}

var add_column = function(matrix, matrix_name, b_add_column, b_del_column) {
    for (var i = 0; i < matrix.length; ++i) {
        matrix[i].push(0);

        var id = matrix_name.id[5] + (i + 1);
        var cell = document.createElement('input');
        if (matrix_name.id == 'matr_c' || document.getElementById('left').style.backgroundColor == "rgb(246, 193, 192)") {
            cell.readOnly = true;
            cell.disabled = true;
            B = document.getElementById('let_B');
            B.style.width = document.getElementById('matr_c').offsetWidth + 'px';
        }
        cell.id = id + ',' + matrix[i].length;
        cell.value = cell.id;
        cell.style.color = "gray";
        cell.onfocus = function() {
            if (this.value == this.id) {
                this.value = "";
                this.style.color = "black";
            }
            this.style.border = '1px solid #529ADB';
            this.style.outline = '1px solid #529ADB';
            this.style.boxShadow = 'inset 0 1px 0 white';
            document.getElementById('left').style.backgroundColor = "#5199DB";
        }
        cell.onblur = function() {
            if (this.value == "") {
                this.value = this.id;
                this.style.color = "gray";
            }
            this.style.border = '1px solid #D9D9D9';
            this.style.outline = '';
            this.style.boxShadow = 'inset 0 1px 0 #B2B2B2';
            document.getElementById('left').style.backgroundColor = "#BCBCBC";
        }
        p = document.getElementById(id);
        p.appendChild(cell);

        if (matrix_name.id == 'matr_c') {
            B = document.getElementById('let_B');
            B.style.width = document.getElementById('matr_c').offsetWidth + 'px';
        }
    }

    if (matrix[0].length == 10) {
        b_add_column.style.color = 'gray';
        b_add_column.style.cursor = 'default';
        b_add_column.id = 'add_column_disable';
        b_add_column.tabIndex = '-1';
    } else {
        b_del_column.style.color = 'black';
        b_del_column.style.cursor = 'pointer';
        b_del_column.id = 'del_column';
        b_del_column.tabIndex = '0';
    
    }
}

var del_column = function(matrix, matrix_name, b_add_column, b_del_column) {
    for (var i = 0; i < matrix.length; ++i) {
        matrix[i].pop();

        var p_id = matrix_name.id[5] + (i + 1);
        var cell_id = p_id +  ',' + (matrix[i].length + 1);
        var p = document.getElementById(p_id);
        var cell = document.getElementById(cell_id);
        p.removeChild(cell);
    }

    if (matrix_name.id == 'matr_c') {
        B = document.getElementById('let_B');
        B.style.width = document.getElementById('matr_c').offsetWidth + 'px';
    }

    if (matrix[0].length == 2) {
        b_del_column.style.color = 'gray';
        b_del_column.style.cursor = 'default';
        b_del_column.id = 'del_column_disable';
        b_del_column.tabIndex = '-1';
    } else {
        b_add_column.style.color = 'black';
        b_add_column.style.cursor = 'pointer';
        b_add_column.id = 'add_column';
        b_add_column.tabIndex = '0';
    }
}

var product_matrices = function(matrix_a, matrix_b, matrix_c) {
    if (matrix_a[0].length != matrix_b.length) {
        return -1;
    }

    for (var i = 0; i < matrix_a.length; ++i) {
        for (var j = 0; j < matrix_b[0].length; ++j) {
            matrix_c[i][j] = 0;
            for (var k = 0; k < matrix_a[0].length; ++k) {
                matrix_c[i][j] += matrix_a[i][k] * matrix_b[k][j];
            }
        }
    }
}

var get_nums_from_cells = function(matrix, matrix_name) {
    var bool = true;
    for (var i = 0; i < matrix.length; ++i) {
        for (var j = 0; j < matrix[0].length; ++j) {
            var id = matrix_name.id[5] + (i + 1) + ',' + (j + 1);
            matrix[i][j] = document.getElementById(id).value;
            if (isNaN(matrix[i][j])) {
                bool = 0;
            }
        }
    }
    if (bool) {
        return 1;
    } else {
        return 0;
    }
}

var fill_cells = function(matrix, matrix_name) {
    for (var i = 0; i < matrix.length; ++i) {
        for (var j = 0; j < matrix[0].length; ++j) {
            var id = matrix_name.id[5] + (i + 1) + ',' + (j + 1);
            var cell = document.getElementById(id);
            if (!isNaN(matrix[i][j])) {
                cell.value = matrix[i][j];
                cell.style.color = "black";
            }
        }
    }
}

var make_cells_disable = function(matrix_a, matrix_b) {
    for (var i = 0; i < matrix_a.length; ++i) {
        for (var j = 0; j < matrix_a[0].length; ++j) {
            var id = 'a' + (i + 1) + ',' + (j + 1);
            var cell = document.getElementById(id);
            cell.readOnly = true;
            cell.disabled = true;
            cell.tabIndex = '-1';
        }
    }

    for (var i = 0; i < matrix_b.length; ++i) {
        for (var j = 0; j < matrix_b[0].length; ++j) {
            var id = 'b' + (i + 1) + ',' + (j + 1);
            var cell = document.getElementById(id);
            cell.readOnly = true;
            cell.disabled = true;
            cell.tabIndex = '-1';
        }
    }
}

var make_cells_readable = function(matrix_a, matrix_b) {
    for (var i = 0; i < matrix_a.length; ++i) {
        for (var j = 0; j < matrix_a[0].length; ++j) {
            var id = 'a' + (i + 1) + ',' + (j + 1);
            var cell = document.getElementById(id);
            cell.readOnly = false;
            cell.disabled = false;
            cell.tabIndex = '0';
        }
    }

    for (var i = 0; i < matrix_b.length; ++i) {
        for (var j = 0; j < matrix_b[0].length; ++j) {
            var id = 'b' + (i + 1) + ',' + (j + 1);
            var cell = document.getElementById(id);
            cell.readOnly = false;
            cell.disabled = false;
            cell.tabIndex = '0';
        }
    }
}

var create_matrixes = function(matrix_a, matrix_b, matrix_c, b_add_row, b_del_row, b_add_column, b_del_column) {
    add_column(matrix_c, matr_c, b_add_column, b_del_column);
    add_column(matrix_c, matr_c, b_add_column, b_del_column);
    add_row(matrix_c, matr_c, b_add_row, b_del_row);

    add_column(matrix_a, matr_a, b_add_column, b_del_column);
    add_column(matrix_a, matr_a, b_add_column, b_del_column);
    add_row(matrix_a, matr_a, b_add_row, b_del_row);

    add_column(matrix_b, matr_b, b_add_column, b_del_column);
    add_column(matrix_b, matr_b, b_add_column, b_del_column);
    add_row(matrix_b, matr_b, b_add_row, b_del_row);

    b_add_row.id = 'add_row_disable';
    b_add_row.tabIndex = '-1';
    b_add_row.style.color = 'gray';
    b_add_row.style.cursor = 'default';

    b_del_row.id = 'del_row_disable';
    b_del_row.tabIndex = '-1';
    b_del_row.style.color = 'gray';
    b_del_row.style.cursor = 'default';

    b_add_column.id = 'add_column_disable';
    b_add_column.tabIndex = '-1';
    b_add_column.style.color = 'gray';
    b_add_column.style.cursor = 'default';

    b_del_column.id = 'del_column_disable';
    b_del_column.tabIndex = '-1';
    b_del_column.style.color = 'gray';
    b_del_column.style.cursor = 'default';
}

var text_error = function() {
    var p = document.createElement("P");
    p.id = "p1";
    var t = document.createTextNode("Такие матрицы нельзя перемножить,");
    p.appendChild(t);
    log.appendChild(p);
    p = document.createElement("P");
    p.id = "p2";
    t = document.createTextNode("так как количество столбцов матрицы А");
    p.appendChild(t);
    log.appendChild(p);
    p = document.createElement("P");
    p.id = "p3";
    t = document.createTextNode("не равно количеству строк матрицы В");
    p.appendChild(t);    
    log.appendChild(p);
}

window.onload = function()
{
    var matrix_a = [[]];
    var matrix_b = [[]];
    var matrix_c = [[]];

    var b_add_row = document.getElementById('add_row');
    var b_del_row = document.getElementById('del_row');
    var b_add_column = document.getElementById('add_column');
    var b_del_column = document.getElementById('del_column');
    var produce = document.getElementById('produce');
    var clear = document.getElementById('clear');
    var rearrange = document.getElementById('rearrange');
    var check_matrix_a = document.getElementById('check_matrix_a');
    var check_matrix_b = document.getElementById('check_matrix_b');
    var check_in_a = document.getElementById('check_in_a');
    var check_in_b = document.getElementById('check_in_b');

    produce.style.cursor = rearrange.style.cursor = clear.style.cursor = 'pointer';
    b_add_row.style.cursor = b_del_row.style.cursor = 'default';
    b_add_column.style.cursor = b_del_column.style.cursor = 'default';

    check_in_a.style.display = 'none';
    check_in_b.style.display = 'none';
    var click_a = false;
    var click_b = false;
    var click_produce = false;
    var produce_onfocus = false;

    b_add_row.onmouseover = function() {
        if (this.style.cursor == 'pointer') {
            b_add_row.id = 'add_row_over';
        }
    };

    b_del_row.onmouseover = function() {
        if (this.style.cursor == 'pointer') {
            b_del_row.id = 'del_row_over';
        }
    };

    b_add_column.onmouseover = function() {
        if (this.style.cursor == 'pointer') {
            b_add_column.id = 'add_column_over';
        }
    };

    b_del_column.onmouseover = function() {
        if (this.style.cursor == 'pointer') {
            b_del_column.id = 'del_column_over';
        }
    };

    clear.onmouseover = function() {
        clear.id = 'clear_over';
    };

    rearrange.onmouseover = function() {
        rearrange.id = 'rearrange_over';
    }

    b_add_row.onmouseout = function() {
        if (this.style.cursor == 'pointer') {
            b_add_row.id = 'add_row';
        }
    };

    b_del_row.onmouseout = function() {
        if (this.style.cursor == 'pointer') {
            b_del_row.id = 'del_row';
        }
    };

    b_add_column.onmouseout = function() {
        if (this.style.cursor == 'pointer') {
            b_add_column.id = 'add_column';
        }
    };

    b_del_column.onmouseout = function() {
        if (this.style.cursor == 'pointer') {
            b_del_column.id = 'del_column';
        }
    };

    clear.onmouseout = function() {
        clear.id = 'clear';
    };

    rearrange.onmouseout = function() {
        rearrange.id ='rearrange';
    };

    clear.onmousedown = function() {
        clear.id = 'clear_click';
    };

    rearrange.onmousedown = function() {
        rearrange.id = 'rearrange_click';
    };

    b_add_row.onmousedown = function() {
        if (this.style.cursor == 'pointer') {
            b_add_row.id = 'add_row_click';
        }
    };

    b_del_row.onmousedown = function() {
        if (this.style.cursor == 'pointer') {
            b_del_row.id = 'del_row_click';
        }
    };

    b_add_column.onmousedown = function() {
        if (this.style.cursor == 'pointer') {
            b_add_column.id = 'add_column_click';
        }
    };

    b_del_column.onmousedown = function() {
        if (this.style.cursor == 'pointer') {
            b_del_column.id = 'del_column_click';
        }
    };

    b_add_row.onmouseup = function() {
        if (this.style.cursor == 'pointer') {
            b_add_row.id = 'add_row_over';
        }
    };

    b_del_row.onmouseup = function() {
        if (this.style.cursor == 'pointer') {
            b_del_row.id = 'del_row_over';
        }
    };

    b_add_column.onmouseup = function() {
        if (this.style.cursor == 'pointer') {
            b_add_column.id ='add_column_over';
        }
    };

    b_del_column.onmouseup = function() {
        if (this.style.cursor == 'pointer') {
            b_del_column.id ='del_column_over';
        }
    };

    clear.onmouseup = function() {
        clear.id = 'clear_over';
    };

    rearrange.onmouseup = function() {
        rearrange.id = 'rearrange_over';
    };

    b_add_row.onkeypress = function(event) {
        if (!event) {
            event = window.event;
        }
        var x = event.which || event.keyCode; 
        if (x == 13) {
            this.id = 'add_row_click';
            this.click();
            this.id = 'add_row';
        }
    };

    b_del_row.onkeypress = function(event) {
        if (!event) {
            event = window.event;
        }
        var x = event.which || event.keyCode; 
        if (x == 13) {
            this.id = 'del_row_click';
            this.click();
            this.id = 'del_row';
        }
    };

    b_add_column.onkeypress = function(event) {
        if (!event) {
            event = window.event;
        }
        var x = event.which || event.keyCode; 
        if (x == 13) {
            this.id = 'add_column_click';
            this.click();
            this.id = 'add_column';
        }
    };

    b_del_column.onkeypress = function(event) {
        if (!event) {
            event = window.event;
        }
        var x = event.which || event.keyCode; 
        if (x == 13) {
            this.id = 'del_column_click';
            this.click();
            this.id = 'del_column';
        }
    };

    clear.onkeypress = function(event) {
        if (!event) {
            event = window.event;
        }
        var x = event.which || event.keyCode; 
        if (x == 13) {
            this.id = 'clear_click';
            this.click();
            this.id = 'clear';
        }
    };

    rearrange.onkeypress = function(event) {
        if (!event) {
            event = window.event;
        }
        var x = event.which || event.keyCode; 
        if (x == 13) {
            this.id = 'rearrange_click';
            this.click();
            this.id = 'rearrange';
        }
    };

    check_matrix_a.onkeypress = function(event) {
        if (!event) {
            event = window.event;
        }
        var x = event.which || event.keyCode; 
        if (x == 13) {
            this.onmousedown();
            click_a = false;
        }
    }


    check_matrix_b.onkeypress = function(event) {
        if (!event) {
            event = window.event;
        }
        var x = event.which || event.keyCode; 
        if (x == 13) {
            this.onmousedown();
            click_b = false;
        }
    }

    b_add_row.onfocus = function() {
        if (b_add_row.id != 'add_row_click') {
            this.style.border = '1px solid #529ADB';
            this.style.outline = '1px solid #529ADB';
        }
    };

    b_del_row.onfocus = function() {
        if (b_del_row.id != 'del_row_click') {
            this.style.border = '1px solid #529ADB';
            this.style.outline = '1px solid #529ADB';
        }
    };

    b_add_column.onfocus = function() {
        if (b_add_column.id != 'add_column_click') {
            this.style.border = '1px solid #529ADB';
            this.style.outline = '1px solid #529ADB';
        }
    };

    b_del_column.onfocus = function() {
        if (b_del_column.id != 'del_column_click') {
            this.style.border = '1px solid #529ADB';
            this.style.outline = '1px solid #529ADB';
        }
    };

    clear.onfocus = function() {
        if (clear.id != 'clear_click') {
            this.style.border = '1px solid #529ADB';
            this.style.outline = '1px solid #529ADB';
        }
    };

    rearrange.onfocus = function() {
        if (rearrange.id != 'rearrange_click') {
            this.style.border = '1px solid #529ADB';
            this.style.outline = '1px solid #529ADB';
        }
    };

    b_add_row.onblur = b_del_row.onblur = function() {
        this.style.border = '1px solid gray';
        this.style.outline = '';
    };

    b_add_column.onblur = b_del_column.onblur = function() {
        this.style.border = '1px solid gray';
        this.style.outline = '';
    };

    clear.onblur = rearrange.onblur = function() {
        this.style.border = '1px solid gray';
        this.style.outline = '';
    };

    check_matrix_a.onfocus = function() {
        this.style.outline = 'none';
        if (!click_a) {
            this.style.border = '2px solid #529ADB';
            this.style.marginLeft = '34px';
            this.style.marginRight = '-1px';
            this.style.marginTop = '-1px';
            this.style.marginBottom = '-1px';
        }
        click_a = false;
    }

    check_matrix_b.onfocus = function() {
        this.style.outline = 'none';
        if (!click_b) {
            this.style.border = '2px solid #529ADB';
            this.style.marginLeft = '17px';
            this.style.marginRight = '-1px';
            this.style.marginTop = '-1px';
            this.style.marginBottom = '-1px';
        }
        click_b = false;
    }

    check_matrix_a.onblur = function() {
        this.style.border = '1px solid gray';
        this.style.outline = 'none';
        this.style.marginLeft = '35px';
        this.style.marginRight = '0px';
        this.style.marginTop = '0px';
        this.style.marginBottom = '0px';
    }

    check_matrix_b.onblur = function() {
        this.style.border = '1px solid gray';
        this.style.outline = 'none';
        this.style.marginLeft = '18px';
        this.style.marginRight = '0px';
        this.style.marginTop = '0px';
        this.style.marginBottom = '0px';
    }

    produce.onfocus = function() {
        this.style.outline = 'none';
        produce_onfocus = true;
        if (!click_produce) {
            this.style.marginTop = '32px';
            this.style.marginLeft = '32px';
            this.style.marginBottom = '52px';
            var button = document.getElementById('button');
            button.style.height = '46px';
            button.style.width = '194px';
            button.src = 'img/button_focus.png';
        }
        click_produce = false;
    }

    produce.onblur = function() {
        var button = document.getElementById('button');
        button.style.height = '40px';
        button.style.width = '189px';
        button.src = 'img/button.png';
        this.style.marginTop = '35px';
        this.style.marginLeft = '35px';
        this.style.marginBottom = '55px';
        produce_onfocus = false;
    }

    check_matrix_a.onmousedown = function() {
        click_a = true;
        check_in_b.style.display = 'none';
        check_in_a.style.display = 'inline-block';
        b_add_row.style.color = 'black';
        b_add_row.style.cursor = 'pointer';
        b_add_row.id = 'add_row';
        b_add_row.tabIndex = '0';

        b_del_row.style.color = 'black';
        b_del_row.style.cursor = 'pointer';
        b_del_row.id = 'del_row';
        b_del_row.tabIndex = '0';

        if (matrix_a.length == 10) {
            b_add_row.style.color = 'gray';
            b_add_row.style.cursor = 'default';
            b_add_row.id = 'add_row_disable';
            b_add_row.tabIndex = '-1';
        } else if (matrix_a.length == 2) {
            b_del_row.style.color = 'gray';
            b_del_row.style.cursor = 'default';
            b_del_row.id = 'del_row_disable';
            b_del_row.tabIndex = '-1';
        }

        b_add_column.style.color = 'black';
        b_add_column.style.cursor = 'pointer';
        b_add_column.id = 'add_column';
        b_add_column.tabIndex = '0';

        b_del_column.style.color = 'black';
        b_del_column.style.cursor = 'pointer';
        b_del_column.id = 'del_column';
        b_del_column.tabIndex = '0';

        if (matrix_a[0].length == 10) {
            b_add_column.style.color = 'gray';
            b_add_column.style.cursor = 'default';
            b_add_column.id = 'add_column_disable';
            b_add_column.tabIndex = '-1';
        } else if (matrix_a[0].length == 2) {
            b_del_column.style.color = 'gray';
            b_del_column.style.cursor = 'default';
            b_del_column.id = 'del_column_disable';
            b_del_column.tabIndex = '-1';
        }
    };

    check_matrix_b.onmousedown = function() {
        click_b = true;
        check_in_a.style.display = 'none';
        check_in_b.style.display = 'inline-block';
        b_add_row.style.color = 'black';
        b_add_row.style.cursor = 'pointer';
        b_add_row.id = 'add_row';
        b_add_row.tabIndex = '0';

        b_del_row.style.color = 'black';
        b_del_row.style.cursor = 'pointer';
        b_del_row.id = 'del_row';
        b_del_row.tabIndex = '0';
        
        if (matrix_b.length == 10) {
            b_add_row.style.color = 'gray';
            b_add_row.style.cursor = 'default';
            b_add_row.id = 'add_row_disable';
            b_add_row.tabIndex = '-1';
        } else if (matrix_b.length == 2) {
            b_del_row.style.color = 'gray';
            b_del_row.style.cursor = 'default';
            b_del_row.id = 'del_row_disable';
            b_del_row.tabIndex = '-1';
        }

        b_add_column.style.color = 'black';
        b_add_column.style.cursor = 'pointer';
        b_add_column.id = 'add_column';
        b_add_column.tabIndex = '0';

        b_del_column.style.color = 'black';
        b_del_column.style.cursor = 'pointer';
        b_del_column.id = 'del_column';
        b_del_column.tabIndex = '0';

        if (matrix_b[0].length == 10) {
            b_add_column.style.color = 'gray';
            b_add_column.style.cursor = 'default';
            b_add_column.id = 'add_column_disable';
            b_add_column.tabIndex = '-1';
        } else if (matrix_b[0].length == 2) {
            b_del_column.style.color = 'gray';
            b_del_column.style.cursor = 'default';
            b_del_column.id = 'del_column_disable';
            b_del_column.tabIndex = '-1';
        }
    };

    b_add_row.onclick = function() {
        if(document.getElementById('check_in_a').style.display == 'inline-block') {
            if (matrix_a.length < 10) {
                add_row(matrix_a, matr_a, b_add_row, b_del_row);
                add_row(matrix_c, matr_c, b_add_row, b_del_row);
            }
        } else if(document.getElementById("check_in_b").style.display == 'inline-block') {
            if (matrix_b.length < 10) {
                add_row(matrix_b, matr_b, b_add_row, b_del_row);
            }
        }

        if (matrix_a[0].length != matrix_b.length) {
            document.getElementById('left').style.backgroundColor = "#F6C1C0";

            if (log.children.length == 0) {
                text_error();
                make_cells_disable(matrix_a, matrix_b);
            }
        } else {
            document.getElementById('left').style.backgroundColor = "#BCBCBC";

            if (log.children.length) {
                log.removeChild(p3);
                log.removeChild(p2);
                log.removeChild(p1);
                make_cells_readable(matrix_a, matrix_b);
            }
        }

        document.getElementById('left').style.height = Math.max(document.getElementById('matrices').offsetHeight, document.body.offsetHeight) + 'px';
    };

    b_del_row.onclick = function() {
        if(document.getElementById('check_in_a').style.display == 'inline-block') {
            if (matrix_a.length > 2) {
                del_row(matrix_a, matr_a, b_add_row, b_del_row);
                del_row(matrix_c, matr_c, b_add_row, b_del_row);
            }
        } else if(document.getElementById("check_in_b").style.display == 'inline-block') {
            if (matrix_b.length > 2) {
                del_row(matrix_b, matr_b, b_add_row, b_del_row);
            }
        }

        if (matrix_a[0].length != matrix_b.length) {
            document.getElementById('left').style.backgroundColor = "#F6C1C0";

            if (log.children.length == 0) {
                text_error();
                make_cells_disable(matrix_a, matrix_b);
            }
        } else {
            document.getElementById('left').style.backgroundColor = "#BCBCBC";

            if (log.children.length) {
                log.removeChild(p3);
                log.removeChild(p2);
                log.removeChild(p1);
                make_cells_readable(matrix_a, matrix_b);
            }
        }

        document.getElementById('left').style.height = Math.max(document.getElementById('matrices').offsetHeight, document.body.offsetHeight) + 'px';
    };

    b_add_column.onclick = function() {
        if(document.getElementById('check_in_a').style.display == 'inline-block') {
            if (matrix_a[0].length < 10) {
                add_column(matrix_a, matr_a, b_add_column, b_del_column);
            }
        } else if(document.getElementById("check_in_b").style.display == 'inline-block') {
            if (matrix_b[0].length < 10) {
                add_column(matrix_b, matr_b, b_add_column, b_del_column);
                add_column(matrix_c, matr_c, b_add_column, b_del_column);
            }
        }

        if (matrix_a[0].length != matrix_b.length) {
            document.getElementById('left').style.backgroundColor = "#F6C1C0";

            if (log.children.length == 0) {
                text_error();
                make_cells_disable(matrix_a, matrix_b);
            }
        } else {
            document.getElementById('left').style.backgroundColor = "#BCBCBC";

            if (log.children.length) {
                log.removeChild(p3);
                log.removeChild(p2);
                log.removeChild(p1);
                make_cells_readable(matrix_a, matrix_b);
            }
        }
    };

    b_del_column.onclick = function() {
        if(document.getElementById('check_in_a').style.display == 'inline-block') {
            if (matrix_a[0].length > 2) {
                del_column(matrix_a, matr_a, b_add_column, b_del_column);
            }
        } else if(document.getElementById("check_in_b").style.display == 'inline-block') {
            if (matrix_b[0].length > 2) {
                del_column(matrix_b, matr_b, b_add_column, b_del_column);
                del_column(matrix_c, matr_c, b_add_column, b_del_column);
            }
        }

        if (matrix_a[0].length != matrix_b.length) {
            document.getElementById('left').style.backgroundColor = "#F6C1C0";

            if (log.children.length == 0) {
                text_error();
                make_cells_disable(matrix_a, matrix_b);
            }
        } else {
            document.getElementById('left').style.backgroundColor = "#BCBCBC";

            if (log.children.length) {
                log.removeChild(p3);
                log.removeChild(p2);
                log.removeChild(p1);
                make_cells_readable(matrix_a, matrix_b);
            }
        }
    };

    produce.onmouseover = function() {
        click_produce = true;
        var button = document.getElementById('button');
        if (produce_onfocus) {
            button.style.height = '46px';
            button.style.width = '194px';
            button.src = 'img/button_focus_over.png';
        } else  {
            button.style.height = '40px';
            button.style.width = '189px';
            button.src = 'img/button_hover.png';
        }
    };

    produce.onmouseout = function() {
        click_produce = false;
        var button = document.getElementById('button');
        if (produce_onfocus) {
            button.src = 'img/button_focus.png';
        } else {
            button.style.height = '40px';
            button.style.width = '189px';
            button.src = 'img/button.png';
        }
    };

    produce.onmousedown = function() {
        click_produce = true;
        var button = document.getElementById('button');
        button.style.height = '40px';
        button.style.width = '189px';
        this.style.marginTop = '35px';
        this.style.marginLeft = '35px';
        this.style.marginBottom = '55px';
        produce_onfocus = false;
        button.src = 'img/button_click.png';
    };

    produce.onmouseup = function() {
        var button = document.getElementById('button');
        button.style.height = '40px';
        button.style.width = '189px';
        button.src = 'img/button.png';
    };

    produce.onclick = function() {
        produce_onfocus = false;
        if (get_nums_from_cells(matrix_a, matr_a) && get_nums_from_cells(matrix_b, matr_b)) {
            product_matrices(matrix_a, matrix_b, matrix_c);
            fill_cells(matrix_c, matr_c);
        } else {
            for (var i = 0; i < matrix_c.length; ++i) {
                for (var j = 0; j < matrix_c[i].length; ++j) {
                    var id = 'c' + (i + 1) + ',' + (j + 1);
                    cell = document.getElementById(id);
                    cell.value = id;
                    cell.style.color = "gray";
                }
            }
        }
    };

    produce.onkeypress = function(event) {
        if (!event) {
            event = window.event;
        }
        var x = event.which || event.keyCode; 
        if (x == 13) {
            if (get_nums_from_cells(matrix_a, matr_a) && get_nums_from_cells(matrix_b, matr_b)) {
                product_matrices(matrix_a, matrix_b, matrix_c);
                fill_cells(matrix_c, matr_c);
            } else {
                for (var i = 0; i < matrix_c.length; ++i) {
                    for (var j = 0; j < matrix_c[i].length; ++j) {
                        var id = 'c' + (i + 1) + ',' + (j + 1);
                        cell = document.getElementById(id);
                        cell.value = id;
                        cell.style.color = "gray";
                    }
                }
            }
        }
    };

    clear.onclick = function() {
        for (var i = 0; i < matrix_a.length; ++i) {
            for (var j = 0; j < matrix_a[i].length; ++j) {
                var id = 'a' + (i + 1) + ',' + (j + 1);
                cell = document.getElementById(id);
                cell.value = id;
                cell.style.color = "gray";
            }
        }

        for (var i = 0; i < matrix_b.length; ++i) {
            for (var j = 0; j < matrix_b[i].length; ++j) {
                var id = 'b' + (i + 1) + ',' + (j + 1);
                cell = document.getElementById(id);
                cell.value = id;
                cell.style.color = "gray";
            }
        }

        for (var i = 0; i < matrix_c.length; ++i) {
            for (var j = 0; j < matrix_c[i].length; ++j) {
                var id = 'c' + (i + 1) + ',' + (j + 1);
                cell = document.getElementById(id);
                cell.value = id;
                cell.style.color = "gray";
            }
        }
    };

    rearrange.onclick = function() {
        var len_a_rows = matrix_a.length;
        var len_a_columns = matrix_a[0].length;
        var len_b_rows = matrix_b.length;
        var len_b_columns = matrix_b[0].length;

        var cp_a = [];
        for (var i = 0; i < matrix_a.length; ++i) {
            var tmp = [];
            for (var j = 0; j < matrix_a[0].length; ++j) {
                tmp.push(0);
            }
            cp_a.push(tmp);
        }
        get_nums_from_cells(cp_a, matr_a);
        for (var i = 0; i < cp_a.length; ++i) {
            for (var j = 0; j < cp_a[0].length; ++j) {
                console.log(cp_a[i][j]);
            }
        }

        var cp_b = [];
        for (var i = 0; i < matrix_b.length; ++i) {
            var tmp = [];
            for (var j = 0; j < matrix_b[0].length; ++j) {
                tmp.push(0);
            }
            cp_b.push(tmp);
        }
        get_nums_from_cells(cp_b, matr_b);

        var len = matrix_a.length;
        for (var i = 0; i < len - 1; ++i) {
            del_row(matrix_a, matr_a, b_add_row, b_del_row);
            del_row(matrix_c, matr_c, b_add_row, b_del_row);
        }

        len = matrix_a[0].length
        for (var i = 0; i < len; ++i) {
            del_column(matrix_a, matr_a, b_add_column, b_del_column);
        }

        len = matrix_b.length;
        for (var i = 0; i < len - 1; ++i) {
            del_row(matrix_b, matr_b, b_add_row, b_del_row);
        }

        len = matrix_b[0].length
        for (var i = 0; i < len; ++i) {
            del_column(matrix_b, matr_b, b_add_column, b_del_column);
            del_column(matrix_c, matr_c, b_add_column, b_del_column);
        }

        matrix_c = [[]];
        matrix_a = [[]];
        matrix_b = [[]];

        for (var i = 0; i < len_a_columns; ++i) {
            add_column(matrix_b, matr_b, b_add_column, b_del_column);
            add_column(matrix_c, matr_c, b_add_column, b_del_column);
        }

        for (var i = 0; i < len_b_rows - 1; ++i) {
            add_row(matrix_a, matr_a, b_add_row, b_del_row);
            add_row(matrix_c, matr_c, b_add_row, b_del_row);
        }

        for (var i = 0; i < len_b_columns; ++i) {
            add_column(matrix_a, matr_a, b_add_column, b_del_column);
        }        

        for (var i = 0; i < len_a_rows - 1; ++i) {
            add_row(matrix_b, matr_b, b_add_row, b_del_row);
        }

        if (matrix_a[0].length != matrix_b.length) {
            document.getElementById('left').style.backgroundColor = "#F6C1C0";

            if (log.children.length == 0) {
                text_error();
                make_cells_disable(matrix_a, matrix_b);
            }
        } else {
            document.getElementById('left').style.backgroundColor = "#BCBCBC";

            if (log.children.length) {
                log.removeChild(p3);
                log.removeChild(p2);
                log.removeChild(p1);
                make_cells_readable(matrix_a, matrix_b);
            }
        }

        fill_cells(cp_a, matr_b);        
        fill_cells(cp_b, matr_a);

        if (document.getElementById('check_in_a').style.display == 'inline-block') {
            
            if (matrix_a.length == 2) {
                b_del_row.id = 'del_row_disable';
                b_del_row.tabIndex = '-1';
                b_del_row.style.color = 'gray';
                b_del_row.style.cursor = 'default';
            } else if (matrix_a.length == 10) {
                b_add_row.id = 'add_row_disable';
                b_add_row.tabIndex = '-1';
                b_add_row.style.color = 'gray';
                b_add_row.style.cursor = 'default';
            }

            if (matrix_a[0].length == 2) {
                b_del_column.id = 'del_column_disable';
                b_del_column.tabIndex = '-1';
                b_del_column.style.color = 'gray';
                b_del_column.style.cursor = 'default';
            } else if (matrix_a[0].length == 10) {
                b_add_column.id = 'add_column_disable';
                b_add_column.tabIndex = '-1';
                b_add_column.style.color = 'gray';
                b_add_column.style.cursor = 'default';
            }

        } else if (document.getElementById('check_in_b').style.display == 'inline-block') {
            
            if (matrix_b.length == 2) {
                b_del_row.id = 'del_row_disable';
                b_del_row.tabIndex = '-1';
                b_del_row.style.color = 'gray';
                b_del_row.style.cursor = 'default';
            } else if (matrix_b.length == 10) {
                b_add_row.id = 'add_row_disable';
                b_add_row.tabIndex = '-1';
                b_add_row.style.color = 'gray';
                b_add_row.style.cursor = 'default';
            }

            if (matrix_b[0].length == 2) {
                b_del_column.id = 'del_column_disable';
                b_del_column.tabIndex = '-1';
                b_del_column.style.color = 'gray';
                b_del_column.style.cursor = 'default';
            } else if (matrix_b[0].length == 10) {
                b_add_column.id = 'add_column_disable';
                b_add_column.tabIndex = '-1';
                b_add_column.style.color = 'gray';
                b_add_column.style.cursor = 'default';
            }

        } else {
            b_add_row.id = 'add_row_disable';
            b_add_row.tabIndex = '-1';
            b_add_row.style.color = 'gray';
            b_add_row.style.cursor = 'default';

            b_del_row.id = 'del_row_disable';
            b_del_row.tabIndex = '-1';
            b_del_row.style.color = 'gray';
            b_del_row.style.cursor = 'default';

            b_add_column.id = 'add_column_disable';
            b_add_column.tabIndex = '-1';
            b_add_column.style.color = 'gray';
            b_add_column.style.cursor = 'default';

            b_del_column.id = 'del_column_disable';
            b_del_column.tabIndex = '-1';
            b_del_column.style.color = 'gray';
            b_del_column.style.cursor = 'default';
        }
    };

    create_matrixes(matrix_a, matrix_b, matrix_c, b_add_row, b_del_row, b_add_column, b_del_column);

    B = document.getElementById('let_B');
    B.style.width = document.getElementById('matr_c').offsetWidth + 'px';
}